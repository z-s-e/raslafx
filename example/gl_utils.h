/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef GL_UTILS_H
#define GL_UTILS_H

#include <glad/gl.h>

namespace gl {

void initialize(GLADloadfunc loader);

class program {
public:
    program() = default;
    program(const char* vertex_src, const char* fragment_src);
    program(program&& p);

    ~program();

    program& operator=(program&& p);

    explicit operator bool() const { return program_name != 0; }

    void reset(const char* vertex_src, const char* fragment_src);
    void clear();

    GLuint name() const { return program_name; }

    program(const program&) = delete;
    program& operator=(const program&) = delete;

private:
    GLuint program_name = 0;
};

class buffer {
public:
    buffer() = default;
    buffer(unsigned size, const void* data, GLbitfield flags, GLuint preallocated_name = 0);
    buffer(buffer&& p);

    ~buffer();

    buffer& operator=(buffer&& p);

    explicit operator bool() const { return d.name != 0; }

    void reset(unsigned size, const void* data, GLbitfield flags, GLuint preallocated_name = 0);
    void clear();

    GLuint name() const { return d.name; }
    unsigned size() const { return d.size; }

    buffer(const buffer&) = delete;
    buffer& operator=(const buffer&) = delete;

private:
    struct data {
        GLuint name = 0;
        unsigned size = 0;
    } d;
};

class vertex_array {
public:
    vertex_array() = default;
    explicit vertex_array(GLuint preallocated_name);
    vertex_array(vertex_array&& p);

    ~vertex_array();

    vertex_array& operator=(vertex_array&& p);

    explicit operator bool() const { return vao_name != 0; }

    void reset(GLuint preallocated_name = 0);
    void clear();

    GLuint name() const { return vao_name; }

    vertex_array(const vertex_array&) = delete;
    vertex_array& operator=(const vertex_array&) = delete;

private:
    GLuint vao_name = 0;
};

class texture2d {
public:
    texture2d() = default;
    texture2d(GLenum internalformat, unsigned width, unsigned height,
              GLsizei mipmap_levels = 1, GLuint preallocated_name = 0);
    texture2d(texture2d&& t);

    ~texture2d();

    texture2d& operator=(texture2d&& t);

    explicit operator bool() const { return d.name != 0; }

    void reset(GLenum internalformat, unsigned width, unsigned height,
               GLsizei mipmap_levels = 1, GLuint preallocated_name = 0);
    void clear();

    GLuint name() const { return d.name; }
    unsigned width() const { return d.width; }
    unsigned height() const { return d.height; }

    texture2d(const texture2d&) = delete;
    texture2d& operator=(const texture2d&) = delete;

private:
    struct data {
        GLuint name = 0;
        unsigned width = 0;
        unsigned height = 0;
    } d;
};

class framebuffer2d {
public:
    framebuffer2d() = default;
    framebuffer2d(GLenum internalformat, GLsizei width, GLsizei height,
                  GLsizei mipmap_levels = 1, GLuint preallocated_name = 0, GLuint preallocated_texture = 0);
    framebuffer2d(framebuffer2d&& fb);

    ~framebuffer2d();

    framebuffer2d& operator=(framebuffer2d&& fb);

    explicit operator bool() const { return d.name != 0; }

    void reset(GLenum internalformat, GLsizei width, GLsizei height,
               GLsizei mipmap_levels = 1, GLuint preallocated_name = 0, GLuint preallocated_texture = 0);
    void clear();

    GLuint name() const { return d.name; }
    const texture2d& texture() const { return d.tex; }

    framebuffer2d(const framebuffer2d&) = delete;
    framebuffer2d& operator=(const framebuffer2d&) = delete;

private:
    struct data {
        GLuint name = 0;
        texture2d tex;
    } d;
};

class buffer_mapper {
public:
    buffer_mapper(GLuint buffer_name, GLenum access) : name(buffer_name), ptr(glMapNamedBuffer(name, access)) {}
    ~buffer_mapper() { glUnmapNamedBuffer(name); }

    void* data() { return ptr; }

    buffer_mapper(const buffer_mapper&) = delete;
    buffer_mapper& operator=(const buffer_mapper&) = delete;

private:
    GLuint name = 0;
    void* ptr = nullptr;
};

class vertex_array_binder {
public:
    explicit vertex_array_binder(GLuint vao_name) { glBindVertexArray(vao_name); }
    ~vertex_array_binder() { glBindVertexArray(0); }

    vertex_array_binder(const vertex_array_binder&) = delete;
    vertex_array_binder& operator=(const vertex_array_binder&) = delete;
};

} // namespace

#endif // GL_UTILS_H
