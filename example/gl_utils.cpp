/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gl_utils.h"

#include <algorithm>
#include <cstdio>
#include <string>

namespace gl {


static const char* shader_type_string(GLenum type)
{
    switch( type ) {
    case GL_FRAGMENT_SHADER: return "Fragment";
    case GL_VERTEX_SHADER:   return "Vertex";
    default:                 return "Unknown shader type";
    }
}

static const char NoLog[] = "(no log available)";

static GLuint compile_shader(GLenum type, const char* src)
{
    auto s = glCreateShader(type);
    glShaderSource(s, 1, &src, nullptr);
    glCompileShader(s);

    GLint ret;
    glGetShaderiv(s, GL_COMPILE_STATUS, &ret);
    if( ret == 0 ) {
        std::string log = NoLog;
        glGetShaderiv(s, GL_INFO_LOG_LENGTH, &ret);
        if( ret > 1 ) {
            log = std::string(static_cast<unsigned>(ret), 0);
            glGetShaderInfoLog(s, ret, nullptr, log.data());
        }
        glDeleteShader(s);
        std::fprintf(stderr, "%s shader compilation failed: %s\n", shader_type_string(type), log.data());
        return 0;
    }
    return s;
}

static bool link_shader(GLuint program)
{
    glLinkProgram(program);

    GLint ret;
    glGetProgramiv(program, GL_LINK_STATUS, &ret);
    if( ret == 0 ) {
        std::string log = NoLog;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &ret);
        if( ret > 1 ) {
            log = std::string(static_cast<unsigned>(ret), 0);
            glGetProgramInfoLog(program, ret, nullptr, log.data());
        }
        std::fprintf(stderr, "Shader linking failed: %s\n", log.data());
        return false;
    }
    return true;
}

static GLuint create_program(const char* vs_src, const char* fs_src)
{
    GLuint vertexShader = compile_shader(GL_VERTEX_SHADER, vs_src);
    if( vertexShader == 0 )
        return 0;

    GLuint fragmentShader = compile_shader(GL_FRAGMENT_SHADER, fs_src);
    if( fragmentShader == 0 ) {
        glDeleteShader(vertexShader);
        return 0;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    bool ok = link_shader(program);
    glDetachShader(program, vertexShader);
    glDetachShader(program, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    if( !ok ) {
        glDeleteProgram(program);
        return 0;
    }
    return program;
}

program::program(const char* vertex_src, const char* fragment_src)
{
    reset(vertex_src, fragment_src);
}

program::program(program&& p)
{
    program_name = p.program_name;
    p.program_name = 0;
}

program::~program()
{
    clear();
}

program& program::operator=(program&& p)
{
    clear();
    program_name = p.program_name;
    p.program_name = 0;
    return *this;
}

void program::clear()
{
    if( program_name != 0 ) {
        glDeleteProgram(program_name);
        program_name = 0;
    }
}

void program::reset(const char* vertex_src, const char* fragment_src)
{
    clear();
    program_name = create_program(vertex_src, fragment_src);
}


buffer::buffer(unsigned size, const void* data, GLbitfield flags, GLuint preallocated_name)
{
    reset(size, data, flags, preallocated_name);
}

buffer::buffer(buffer&& p)
{
    d = p.d;
    p.d.name = 0;
}

buffer::~buffer()
{
    clear();
}

buffer& buffer::operator=(buffer&& p)
{
    clear();
    d = p.d;
    p.d.name = 0;
    return *this;
}

void buffer::reset(unsigned size, const void* data, GLbitfield flags, GLuint preallocated_name)
{
    clear();

    if( preallocated_name != 0 )
        d.name = preallocated_name;
    else
        glCreateBuffers(1, &d.name);
    d.size = size;

    glNamedBufferStorage(d.name, d.size, data, flags);
}

void buffer::clear()
{
    if( d.name != 0 ) {
        glDeleteBuffers(1, &d.name);
        d = {};
    }
}


vertex_array::vertex_array(GLuint preallocated_name)
{
    reset(preallocated_name);
}

vertex_array::vertex_array(vertex_array&& p)
{
    vao_name = p.vao_name;
    p.vao_name = 0;
}

vertex_array::~vertex_array()
{
    clear();
}

vertex_array& vertex_array::operator=(vertex_array&& p)
{
    clear();
    vao_name = p.vao_name;
    p.vao_name = 0;
    return *this;
}

void vertex_array::reset(GLuint preallocated_name)
{
    clear();

    if( preallocated_name != 0 )
        vao_name = preallocated_name;
    else
        glCreateVertexArrays(1, &vao_name);
}

void vertex_array::clear()
{
    if( vao_name != 0 ) {
        glDeleteVertexArrays(1, &vao_name);
        vao_name = 0;
    }
}


texture2d::texture2d(GLenum internalformat, unsigned width, unsigned height, GLsizei mipmap_levels, GLuint preallocated_name)
{
    reset(internalformat, width, height, mipmap_levels, preallocated_name);
}

texture2d::texture2d(texture2d&& t)
{
    d = t.d;
    t.d.name = 0;
}

texture2d::~texture2d()
{
    clear();
}

texture2d& texture2d::operator=(texture2d&& t)
{
    clear();
    d = t.d;
    t.d.name = 0;
    return *this;
}

void texture2d::reset(GLenum internalformat, unsigned int width, unsigned int height, GLsizei mipmap_levels, GLuint preallocated_name)
{
    clear();

    if( preallocated_name != 0 )
        d.name = preallocated_name;
    else
        glCreateTextures(GL_TEXTURE_2D, 1, &d.name);

    d.width = width;
    d.height = height;

    glTextureStorage2D(d.name, mipmap_levels, internalformat, d.width, d.height);
}

void texture2d::clear()
{
    if( d.name != 0 )
        glDeleteTextures(1, &d.name);
    d = {};
}


framebuffer2d::framebuffer2d(GLenum internalformat, GLsizei width, GLsizei height,
                             GLsizei mipmap_levels, GLuint preallocated_name, GLuint preallocated_texture)
{
    reset(internalformat, width, height, mipmap_levels, preallocated_name, preallocated_texture);
}

framebuffer2d::framebuffer2d(framebuffer2d&& fb)
{
    d = std::move(fb.d);
    fb.d.name = 0;
}

framebuffer2d::~framebuffer2d()
{
    clear();
}

framebuffer2d& framebuffer2d::operator=(framebuffer2d&& fb)
{
    clear();
    d = std::move(fb.d);
    fb.d.name = 0;
    return *this;
}

void framebuffer2d::clear()
{
    if( d.name != 0 ) {
        glDeleteFramebuffers(1, &d.name);
        d.name = 0;
        d.tex.clear();
    }
}

void framebuffer2d::reset(GLenum internalformat, GLsizei width, GLsizei height,
                          GLsizei mipmap_levels, GLuint preallocated_name, GLuint preallocated_texture)
{
    if( preallocated_name != 0 ) {
        clear();
        d.name = preallocated_name;
    } else if( d.name == 0 ) {
        glCreateFramebuffers(1, &d.name);
    }

    d.tex.reset(internalformat, width, height, mipmap_levels, preallocated_texture);
    glClearTexImage(d.tex.name(), 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

    glNamedFramebufferTexture(d.name, GL_COLOR_ATTACHMENT0, d.tex.name(), 0);
}


#ifdef GL_CALLBACK
#error "GL_CALLBACK already defined?"
#endif
#define GL_CALLBACK GLAPIENTRY __attribute__ ((force_align_arg_pointer))

static void GL_CALLBACK gl_debug_cb(GLenum /*source*/, GLenum type, GLuint /*id*/,
                                    GLenum severity, GLsizei /*length*/, const GLchar* message,
                                    const void* /*userParam*/ )
{
    if( severity == GL_DEBUG_SEVERITY_NOTIFICATION )
        return;

    const char* t =
        "Unknown     ";
    switch( type ) {
    case GL_DEBUG_TYPE_ERROR:
        t = "Error       ";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        t = "Deprecated  ";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        t = "Undefined   ";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        t = "Portability ";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        t = "Performace  ";
        break;
    case GL_DEBUG_TYPE_OTHER:
        t = "Other       ";
        break;
    case GL_DEBUG_TYPE_MARKER:
        t = "Marker      ";
        break;
    case GL_DEBUG_TYPE_PUSH_GROUP:
        t = "Push Group  ";
        break;
    case GL_DEBUG_TYPE_POP_GROUP:
        t = "Pop Group   ";
        break;
    default:
        t = "???         ";
    }

    std::fprintf(stderr, "GL CALLBACK: %s, severity = 0x%x, message = %s\n", t, severity, message);
}

void initialize(GLADloadfunc loader)
{
    if( gladLoadGL(loader) == 0 || GLAD_GL_VERSION_4_6 == 0 ) {
        std::fprintf(stderr, "Cannot load GL entry points\n");
        std::abort();
    }

#ifndef NDEBUG
    if( glDebugMessageCallback != nullptr ) {
        glEnable(GL_DEBUG_OUTPUT);
        //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(gl_debug_cb, 0);
    } else {
        std::fprintf(stderr, "OpenGL debug message callback not available\n");
    }
#endif
}

} // namespace
