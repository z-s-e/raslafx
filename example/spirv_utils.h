/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SPIRV_UTILS_H
#define SPIRV_UTILS_H

#include <raslafx/common_types.h>

constexpr unsigned StaticShaderUBOBinding = 0;
constexpr unsigned StaticShaderPushBinding = 1;

void initialize_glslang();

raslafx::spirv_shader compile_spirv(const char* vulkan_glsl, raslafx::ShaderStage glsl_stage);

std::string transpile_spirv_to_opengl_glsl(const raslafx::spirv_shader& spirv);

#endif // SPIRV_UTILS_H
