/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef GL_PIPELINE_H
#define GL_PIPELINE_H

struct size2d {
    unsigned width = 0;
    unsigned height = 0;
};

struct gl_pipeline_internal;

class gl_pipeline
{
public:
    using generic_proc = void (*)(void);
    using gl_loader = generic_proc(*)(const char *);

    gl_pipeline(gl_loader loader, const char* preset);
    ~gl_pipeline();

    static int input_fb_format();

    int begin_frame(size2d input_size);
    void finish_frame(size2d final_viewport_size);

private:
    gl_pipeline_internal* d = nullptr;
};

inline bool operator ==(size2d l, size2d r) { return l.width == r.width && l.height == r.height; }
inline bool operator !=(size2d l, size2d r) { return !(l == r); }

#endif // GL_PIPELINE_H
