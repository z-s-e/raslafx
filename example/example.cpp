/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gl_pipeline.h"

#include <SDL.h>

#include <mpv/client.h>
#include <mpv/render_gl.h>

static Uint32 wakeup_on_mpv_render_update;

static constexpr size2d PipelineInputSize { 320, 180 };
static constexpr size2d InitialWindowSize = { 1280, 720 };

static void die(const char* msg)
{
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "%s", msg);
    auto sdlerror = SDL_GetError();
    if( sdlerror )
        SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "- SDL: %s", sdlerror);
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", msg, nullptr);
    exit(1);
}

static gl_pipeline::generic_proc get_proc_address_glad(const char* name)
{
    return reinterpret_cast<gl_pipeline::generic_proc>(SDL_GL_GetProcAddress(name));
}

static void* get_proc_address_mpv(void*, const char* name)
{
    return SDL_GL_GetProcAddress(name);
}

static void on_mpv_render_update(void *)
{
    SDL_Event event = {};
    event.type = wakeup_on_mpv_render_update;
    SDL_PushEvent(&event);
}

int main(int argc, char* argv[])
{
    if( argc != 3 )
        die("usage: raslafx-example <path-to-video-file> <path-to-slang-preset>");

    setenv("SDL_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR", "0", 0);

    if( SDL_Init(SDL_INIT_VIDEO) < 0 )
        die("SDL init failed");

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

#ifndef NDEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    SDL_Window* window = SDL_CreateWindow("Raslafx-Example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          InitialWindowSize.width, InitialWindowSize.height, SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
    if( !window )
        die("failed to create SDL window");

    SDL_GLContext glcontext = SDL_GL_CreateContext(window);
    if( !glcontext )
        die("failed to create SDL GL context");

    SDL_GL_SetSwapInterval(1);
    SDL_GL_MakeCurrent(window, glcontext);

    auto pipeline = new gl_pipeline(get_proc_address_glad, argv[2]);
    const int pipeline_input_fb_format = pipeline->input_fb_format();

    auto mpv = mpv_create();
    if( !mpv )
        die("context init failed");

    if( mpv_initialize(mpv) < 0 )
        die("mpv init failed");

    mpv_render_context* mpv_gl;
    {
        mpv_opengl_init_params gl_init_params{get_proc_address_mpv, nullptr};
        int advanced = 1;
        mpv_render_param params[]{
            {MPV_RENDER_PARAM_API_TYPE, const_cast<char *>(MPV_RENDER_API_TYPE_OPENGL)},
            {MPV_RENDER_PARAM_OPENGL_INIT_PARAMS, &gl_init_params},
            {MPV_RENDER_PARAM_ADVANCED_CONTROL, &advanced},
            {MPV_RENDER_PARAM_INVALID, nullptr}
        };

        if( mpv_render_context_create(&mpv_gl, mpv, params) < 0 )
            die("could not initialize OpenGL");
    }

    wakeup_on_mpv_render_update = SDL_RegisterEvents(1);
    if( wakeup_on_mpv_render_update == (Uint32)-1 )
        die("could not register events");
    mpv_render_context_set_update_callback(mpv_gl, on_mpv_render_update, nullptr);

    if( mpv_set_property_string(mpv, "demuxer-lavf-o", "video_size=1280x720,input_format=mjpeg") != 0 )
        die("could not set av demuxer option");

    {
        const char *cmd[] = {"loadfile", argv[1], nullptr};
        mpv_command_async(mpv, 0, cmd);
    }

    while( 1 ) {
        SDL_Event event;
        if( SDL_WaitEvent(&event) != 1 )
            die("event loop error");
        bool redraw = false;

        switch( event.type ) {
        case SDL_QUIT:
            goto done;
        case SDL_WINDOWEVENT:
            if( event.window.event == SDL_WINDOWEVENT_EXPOSED )
                redraw = true;
            break;
        default:
            if( event.type == wakeup_on_mpv_render_update ) {
                uint64_t flags = mpv_render_context_update(mpv_gl);
                if( flags & MPV_RENDER_UPDATE_FRAME )
                    redraw = true;
            }
        }
        if( redraw ) {
            int w, h;
            SDL_GetWindowSize(window, &w, &h);
            if( w < 1 || h < 1 )
                die("bad window size?");

            const auto fbo_name = pipeline->begin_frame(PipelineInputSize);
            mpv_opengl_fbo mpv_fbo{fbo_name, PipelineInputSize.width, PipelineInputSize.height, pipeline_input_fb_format};
            //int flip_y{1};
            mpv_render_param params[] = {
                {MPV_RENDER_PARAM_OPENGL_FBO, &mpv_fbo},
                //{MPV_RENDER_PARAM_FLIP_Y, &flip_y},
                {MPV_RENDER_PARAM_INVALID, nullptr}
            };
            mpv_render_context_render(mpv_gl, params);

            pipeline->finish_frame({unsigned(w), unsigned(h)});

            SDL_GL_SwapWindow(window);
        }
    }

done:
    mpv_render_context_free(mpv_gl);
    mpv_terminate_destroy(mpv);

    delete pipeline;

    SDL_GL_DeleteContext(glcontext);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
