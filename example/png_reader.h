/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PNG_READER_H
#define PNG_READER_H

#include <stdint.h>
#include <vector>

using RGBA8888 = uint32_t;

struct Image {
    bool ok = false;
    unsigned width = 0;
    unsigned height = 0;
    bool alpha = false;
    std::vector<RGBA8888> data;
};

Image read_png_file(const char* path);

#endif // PNG_READER_H
