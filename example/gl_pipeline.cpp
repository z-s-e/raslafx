/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gl_pipeline.h"

#include "gl_utils.h"
#include "png_reader.h"
#include "spirv_utils.h"

#include <raslafx/raslafx.h>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <stdint.h>
#include <vector>


static const char FinalOutputVertex[] =
u8R"(#version 450
layout (location = 1) in vec2 TexCoord;
layout (location = 0) out vec2 vTexCoord;

void main()
{
    vTexCoord = vec2(TexCoord.x, 1.0 - TexCoord.y);
    gl_Position = vec4(TexCoord * 2.0 - 1.0, 0.0, 1.0);
}
)";

static const char FinalOutputFragment[] =
u8R"(#version 450
layout (location = 0) in vec2 vTexCoord;
layout (location = 0) out vec4 fragColor;

layout (binding = 0) uniform sampler2D tex;

void main()
{
    fragColor = texture(tex, vTexCoord);
}
)";

static void setup_vao(gl::vertex_array& vao)
{
    vao.reset();
    const auto vao_name = vao.name();

    glVertexArrayAttribFormat(vao_name, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao_name, 0, 0);
    glEnableVertexArrayAttrib(vao_name, 0);

    glVertexArrayAttribFormat(vao_name, 1, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao_name, 1, 0);
    glEnableVertexArrayAttrib(vao_name, 1);

    constexpr GLfloat vertexData[] = {
        0.0,  0.0,
        1.0, 0.0,
        0.0,  1.0,
        1.0, 1.0,
    };
    gl::buffer quad(sizeof(vertexData), vertexData, 0);
    glVertexArrayVertexBuffer(vao_name, 0, quad.name(), 0, 2 * sizeof(GLfloat));
}

static unsigned int compute_mip_levels(unsigned width, unsigned height)
{
    unsigned size = width < height ? height : width;
    unsigned levels = 0;
    while( size ) {
        levels++;
        size >>= 1;
    }
    return levels;
}

static void setup_texture_sample_options(GLuint tex_name, raslafx::texture_sample_options opt)
{
    glTextureParameteri(tex_name, GL_TEXTURE_MAG_FILTER, opt.linear_interpolation ? GL_LINEAR : GL_NEAREST);

    if( opt.mipmap ) {
        glTextureParameteri(tex_name, GL_TEXTURE_MIN_FILTER,
                            opt.linear_interpolation ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST);
    } else {
        glTextureParameteri(tex_name, GL_TEXTURE_MIN_FILTER, opt.linear_interpolation ? GL_LINEAR : GL_NEAREST);
    }

    GLenum wrap = GL_CLAMP_TO_BORDER;
    switch( opt.wrap ) {
    case raslafx::texture_sample_options::WrapMode::Border:
        wrap = GL_CLAMP_TO_BORDER;
        break;
    case raslafx::texture_sample_options::WrapMode::Edge:
        wrap = GL_CLAMP_TO_EDGE;
        break;
    case raslafx::texture_sample_options::WrapMode::Repeat:
        wrap = GL_REPEAT;
        break;
    case raslafx::texture_sample_options::WrapMode::MirroredRepeat:
        wrap = GL_MIRRORED_REPEAT;
        break;
    }

    glTextureParameteri(tex_name, GL_TEXTURE_WRAP_S, wrap);
    glTextureParameteri(tex_name, GL_TEXTURE_WRAP_T, wrap);
}

static gl::texture2d create_user_texture(const raslafx::user_texture& t)
{
    gl::texture2d result;

    const auto img = read_png_file(t.path.data());
    if( !img.ok )
        return {};

    result.reset(img.alpha ? GL_RGBA8 : GL_RGB8, img.width, img.height,
                 t.sample_options.mipmap ? compute_mip_levels(img.width, img.height) : 1);
    setup_texture_sample_options(result.name(), t.sample_options);

    glTextureSubImage2D(result.name(), 0, 0, 0, result.width(), result.height(), GL_RGBA, GL_UNSIGNED_BYTE, img.data.data());
    if( t.sample_options.mipmap )
        glGenerateTextureMipmap(result.name());

    return result;
}

static GLenum gl_framebuffer_format(raslafx::Format f)
{
    using F = raslafx::Format;
    switch( f ) {
    case F::R8_UNORM:                   return GL_R8;
    case F::R8_UINT:                    return GL_R8UI;
    case F::R8_SINT:                    return GL_R8I;
    case F::R8G8_UNORM:                 return GL_RG8;
    case F::R8G8_UINT:                  return GL_RG8UI;
    case F::R8G8_SINT:                  return GL_RG8I;
    case F::R8G8B8A8_UNORM:             return GL_RGBA8;
    case F::R8G8B8A8_UINT:              return GL_RGBA8UI;
    case F::R8G8B8A8_SINT:              return GL_RGBA8I;
    case F::R8G8B8A8_SRGB:              return GL_SRGB8_ALPHA8;
    case F::A2B10G10R10_UNORM_PACK32:   return GL_RGB10_A2;
    case F::A2B10G10R10_UINT_PACK32:    return GL_RGB10_A2UI;
    case F::R16_UINT:                   return GL_R16UI;
    case F::R16_SINT:                   return GL_R16I;
    case F::R16_SFLOAT:                 return GL_R16F;
    case F::R16G16_UINT:                return GL_RG16UI;
    case F::R16G16_SINT:                return GL_RG16I;
    case F::R16G16_SFLOAT:              return GL_RG16F;
    case F::R16G16B16A16_UINT:          return GL_RGBA16UI;
    case F::R16G16B16A16_SINT:          return GL_RGBA16I;
    case F::R16G16B16A16_SFLOAT:        return GL_RGBA16F;
    case F::R32_UINT:                   return GL_R32UI;
    case F::R32_SINT:                   return GL_R32I;
    case F::R32_SFLOAT:                 return GL_R32F;
    case F::R32G32_UINT:                return GL_RG32UI;
    case F::R32G32_SINT:                return GL_RG32I;
    case F::R32G32_SFLOAT:              return GL_RG32F;
    case F::R32G32B32A32_UINT:          return GL_RGBA32UI;
    case F::R32G32B32A32_SINT:          return GL_RGBA32I;
    case F::R32G32B32A32_SFLOAT:        return GL_RGBA32F;
    }
}

static void setup_framebuffer(gl::framebuffer2d& fb, GLenum format, size2d size, raslafx::texture_sample_options sample_options)
{
    fb.reset(format, size.width, size.height, sample_options.mipmap ? compute_mip_levels(size.width, size.height) : 1);
    setup_texture_sample_options(fb.texture().name(), sample_options);
}

static unsigned compute_output_dimension(raslafx::render_pass_output_scaling scaling,
                                         unsigned source_dim, unsigned viewport_dim, bool last)
{
    float base = source_dim;

    using M = raslafx::render_pass_output_scaling::Mode;
    switch( scaling.mode ) {
    case M::Default:
        base = last ? viewport_dim : source_dim;
        break;
    case M::Source:
        break;
    case M::Absolute:
        base = 1.0;
        break;
    case M::Viewport:
        base = viewport_dim;
        break;
    }

    return std::ceil(base * scaling.value);
}

static size2d compute_output_size(raslafx::render_pass_output_scaling x_scaling, raslafx::render_pass_output_scaling y_scaling,
                                  size2d source_size, size2d viewport_size, bool last)
{
    return {compute_output_dimension(x_scaling, source_size.width, viewport_size.width, last),
            compute_output_dimension(y_scaling, source_size.height, viewport_size.height, last)};
}

static size2d texture_size(const gl::texture2d& tex) { return {tex.width(), tex.height()}; }

static void write_texture_size_uniform(void* dst, unsigned offset, size2d size)
{
    float w = size.width;
    float h = size.height;
    GLfloat u[] = { w, h, GLfloat(1.0 / w), GLfloat(1.0 / h) };
    std::memcpy(static_cast<char*>(dst) + offset, u, sizeof(u));
}

static void draw_quad(GLuint framebuffer_name, size2d framebuffer_size, GLuint program_name)
{
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_name);
    glViewport(0, 0, framebuffer_size.width, framebuffer_size.height);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program_name);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}


struct render_pass {
    gl::program program;

    unsigned frame_count_mod = 0;

    struct pass_texture_ref {
        unsigned pass_index = 999; // pass 0 is the pseudo-input pass, pass > 0 may only have history < 2 (history == 1 -> feedback)
        unsigned history = 0;
    };

    struct uniform {
        struct pass_texture_size_uniform {
            pass_texture_ref ref;
            int offset = -1;
        };

        gl::buffer buffer;
        int output_size_offset = -1;
        int final_viewport_size_offset = -1;
        int frame_count_offset = -1;
        std::vector<pass_texture_size_uniform> pass_texture_size_uniforms;

        bool dynamic_update() const {
            return output_size_offset >= 0 || final_viewport_size_offset >=0
                   || frame_count_offset >=0 || !pass_texture_size_uniforms.empty();
        }
    };

    uniform ubo;
    uniform push;

    struct user_texture_binding {
        GLuint tex_name = 0;
        int binding = -1;
    };
    std::vector<user_texture_binding> user_texture_bindings;

    struct pass_texture_binding {
        pass_texture_ref ref;
        int binding = -1;
    };
    std::vector<pass_texture_binding> pass_texture_bindings;

    gl::framebuffer2d output;
    gl::framebuffer2d feedback;

    GLenum format = GL_RGBA8;
    raslafx::render_pass_output_scaling x_scaling;
    raslafx::render_pass_output_scaling y_scaling;
    raslafx::texture_sample_options sample_options;
};

struct uniform_member_initializer {
    render_pass::uniform& uniform;
    char* ptr = nullptr;
    int offset = -1;
    const std::unordered_map<std::string, raslafx::parameter_info>& params;
    const std::vector<gl::texture2d>& textures;
    const std::unordered_map<std::string, size_t>& indices;

    uniform_member_initializer(render_pass::uniform& u, void* data_ptr, const raslafx::uniform_info& member,
                               const std::unordered_map<std::string, raslafx::parameter_info>& parameters,
                               const std::vector<gl::texture2d>& user_textures,
                               const std::unordered_map<std::string, size_t>& user_texture_indices)
        : uniform(u), ptr(static_cast<char*>(data_ptr)), offset(member.offset),
          params(parameters), textures(user_textures), indices(user_texture_indices)
    {
        std::visit(*this, member.data);
    }

    void operator()(const raslafx::uniform_data_mvp&)
    {
        constexpr GLfloat mvp[] = {
            2.0,  0.0,  0.0,  0.0,
            0.0,  2.0,  0.0,  0.0,
            0.0,  0.0,  2.0,  0.0,
            -1.0, -1.0,  0.0,  1.0
        };
        std::memcpy(ptr + offset, mvp, sizeof(mvp));
    }
    void operator()(const raslafx::uniform_data_output_size&)
    {
        uniform.output_size_offset = offset;
    }
    void operator()(const raslafx::uniform_data_final_viewport_size&)
    {
        uniform.final_viewport_size_offset = offset;
    }
    void operator()(const raslafx::uniform_data_frame_count&)
    {
        uniform.frame_count_offset = offset;
    }
    void operator()(const raslafx::uniform_data_frame_direction&)
    {
        const GLint dir = 1;
        std::memcpy(ptr + offset, &dir, sizeof(dir));
    }
    void operator()(const raslafx::uniform_data_parameter& p)
    {
        const GLfloat val = params.at(p.name).initial;
        std::memcpy(ptr + offset, &val, sizeof(val));
    }
    void operator()(const raslafx::uniform_data_texture_size& t)
    {
        if( std::holds_alternative<raslafx::shader_texture_source_original>(t.source) ) {
            const auto& orig = std::get<raslafx::shader_texture_source_original>(t.source);
            uniform.pass_texture_size_uniforms.push_back({{0, orig.history}, offset});
        } else if( std::holds_alternative<raslafx::shader_texture_source_pass_output>(t.source) ) {
            const auto& pass_output = std::get<raslafx::shader_texture_source_pass_output>(t.source);
            uniform.pass_texture_size_uniforms.push_back({{pass_output.pass_index + 1, pass_output.feedback ? 1u : 0u}, offset});
        } else {
            const auto& name = std::get<raslafx::shader_texture_source_user>(t.source).name;
            write_texture_size_uniform(ptr, offset, texture_size(textures.at(indices.at(name))));
        }
    }
};


struct gl_pipeline_internal {
    std::vector<gl::framebuffer2d> input;
    raslafx::texture_sample_options input_sample_options;

    std::vector<gl::texture2d> user_textures;

    std::vector<render_pass> passes;

    gl::vertex_array vao;
    gl::program output_shader;

    uint64_t frame_count = 0;


    gl::framebuffer2d& input_history(unsigned history)
    {
        const auto current = frame_count % input.size();
        const auto idx = (current >= history ? current - history : input.size() - (history - current) );
        return input[idx];
    }

    const gl::texture2d& resolve_pass_texture_ref(render_pass::pass_texture_ref ref)
    {
        if( ref.pass_index == 0 )
            return input_history(ref.history).texture();
        auto& p = passes[ref.pass_index - 1];
        return ref.history == 0 ? p.output.texture() : p.feedback.texture();
    }

    size2d source_size(size_t pass_index)
    {
        return texture_size(pass_index == 0 ? input_history(0).texture() : passes[pass_index - 1].output.texture());
    }

    void update_uniform(const render_pass::uniform& u, size_t pass_index, size2d output_size, size2d final_viewport_size)
    {
        if( ! u.dynamic_update() )
            return;

        gl::buffer_mapper map(u.buffer.name(), GL_WRITE_ONLY);

        if( u.output_size_offset >= 0 )
            write_texture_size_uniform(map.data(), u.output_size_offset, output_size);
        if( u.final_viewport_size_offset >= 0 )
            write_texture_size_uniform(map.data(), u.final_viewport_size_offset, final_viewport_size);
        if( u.frame_count_offset >= 0 ) {
            const auto mod = passes[pass_index].frame_count_mod;
            GLuint pass_frame_count(mod > 0 ? (frame_count % mod) : frame_count);
            std::memcpy(static_cast<char*>(map.data()) + u.frame_count_offset, &pass_frame_count, sizeof(pass_frame_count));
        }

        for( const auto& pass_size : u.pass_texture_size_uniforms )
            write_texture_size_uniform(map.data(), pass_size.offset, texture_size(resolve_pass_texture_ref(pass_size.ref)));
    }

    void initialize_uniform(render_pass::uniform& u, const raslafx::uniform_buffer_info& uniform_info,
                            const std::unordered_map<std::string, raslafx::parameter_info>& params,
                            const std::unordered_map<std::string, size_t>& user_texture_indices)
    {
        if( uniform_info.size == 0 )
            return;

        u.buffer.reset(uniform_info.size, nullptr, GL_MAP_WRITE_BIT);
        gl::buffer_mapper map(u.buffer.name(), GL_WRITE_ONLY);

        for( const auto& mem : uniform_info.members )
            uniform_member_initializer(u, map.data(), mem, params, user_textures, user_texture_indices);
    }

    gl_pipeline_internal(const raslafx::pipeline_description& pipeline)
    {
        input_sample_options = pipeline.input_sample_options;
        input.resize(pipeline.history_required + 1);
        for( size_t i = 1; i < input.size(); ++i )
            setup_framebuffer(input[i], gl_pipeline::input_fb_format(), {1, 1}, input_sample_options);

        std::unordered_map<std::string, size_t> user_texture_indices;
        user_textures.reserve(pipeline.user_textures.size());
        for( auto it = pipeline.user_textures.cbegin(), end = pipeline.user_textures.cend(); it != end; ++it ) {
            const auto idx = user_textures.size();
            user_textures.emplace_back(create_user_texture(it->second));
            user_texture_indices[it->first] = idx;
        }

        const auto pass_count = pipeline.passes.size();
        passes.resize(pass_count);
        for( size_t i = 0; i < pass_count; ++i ) {
            auto& pass_description = pipeline.passes[i];
            auto& pass = passes[i];

            pass.program.reset(transpile_spirv_to_opengl_glsl(pass_description.vertex).data(),
                               transpile_spirv_to_opengl_glsl(pass_description.fragment).data());

            pass.frame_count_mod = pass_description.frame_count_mod;

            initialize_uniform(pass.ubo, pass_description.input.ubo, pipeline.parameters, user_texture_indices);
            initialize_uniform(pass.push, pass_description.input.push_constant, pipeline.parameters, user_texture_indices);

            for( const auto& t : pass_description.input.textures ) {
                if( std::holds_alternative<raslafx::shader_texture_source_original>(t.source) ) {
                    const auto& orig = std::get<raslafx::shader_texture_source_original>(t.source);
                    pass.pass_texture_bindings.push_back({{0, orig.history}, t.binding});
                } else if( std::holds_alternative<raslafx::shader_texture_source_pass_output>(t.source) ) {
                    const auto& pass_output = std::get<raslafx::shader_texture_source_pass_output>(t.source);
                    pass.pass_texture_bindings.push_back({{pass_output.pass_index + 1, pass_output.feedback ? 1u : 0u}, t.binding});
                } else {
                    const auto& name = std::get<raslafx::shader_texture_source_user>(t.source).name;
                    const auto tex_name = user_textures[user_texture_indices[name]].name();
                    pass.user_texture_bindings.push_back({tex_name, t.binding});
                }
            }

            pass.format = gl_framebuffer_format(pass_description.format);
            pass.x_scaling = pass_description.x_scaling;
            pass.y_scaling = pass_description.y_scaling;
            pass.sample_options = pass_description.sample_options;

#if 0
            if( i == pass_count - 1 )
                pass.sample_options.linear_interpolation = true;
#endif

            if( pass_description.feedback_required )
                setup_framebuffer(pass.feedback, pass.format, {1, 1}, pass.sample_options);
        }

        setup_vao(vao);
        output_shader.reset(FinalOutputVertex, FinalOutputFragment);
    }
};


gl_pipeline::gl_pipeline(gl_loader loader, const char* preset)
{
    gl::initialize(loader);
    initialize_glslang();

    auto p = raslafx::load_slang_preset(preset, compile_spirv);
    if( !p )
        abort();
    d = new gl_pipeline_internal(*p);
}

gl_pipeline::~gl_pipeline()
{
    delete d;
}

int gl_pipeline::input_fb_format()
{
    return GL_RGB8;
}

int gl_pipeline::begin_frame(size2d input_size)
{
    auto& fb = d->input_history(0);
    if( texture_size(fb.texture()) != input_size )
        setup_framebuffer(fb, input_fb_format(), input_size, d->input_sample_options);
    return fb.name();
}

void gl_pipeline::finish_frame(size2d final_viewport_size)
{
    if( d->input_sample_options.mipmap )
        glGenerateTextureMipmap(d->input_history(0).texture().name());

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    gl::vertex_array_binder vao_binder(d->vao.name());

    const auto pass_count = d->passes.size();
    for( size_t i = 0; i < pass_count; ++i ) {
        auto& pass = d->passes[i];
        const auto pass_output_size = compute_output_size(pass.x_scaling, pass.y_scaling,
                                                          d->source_size(i), final_viewport_size, i == pass_count - 1);
        if( pass_output_size != texture_size(pass.output.texture()) )
            setup_framebuffer(pass.output, pass.format, pass_output_size, pass.sample_options);

        d->update_uniform(pass.ubo, i, pass_output_size, final_viewport_size);
        d->update_uniform(pass.push, i, pass_output_size, final_viewport_size);

        if( pass.ubo.buffer )
            glBindBufferRange(GL_UNIFORM_BUFFER, StaticShaderUBOBinding, pass.ubo.buffer.name(), 0, pass.ubo.buffer.size());
        if( pass.push.buffer )
            glBindBufferRange(GL_UNIFORM_BUFFER, StaticShaderPushBinding, pass.push.buffer.name(), 0, pass.push.buffer.size());

        for( const auto& tex : pass.pass_texture_bindings )
            glBindTextureUnit(tex.binding, d->resolve_pass_texture_ref(tex.ref).name());
        for( const auto& tex : pass.user_texture_bindings )
            glBindTextureUnit(tex.binding, tex.tex_name);

        if( pass.format == GL_SRGB8_ALPHA8 )
            glEnable(GL_FRAMEBUFFER_SRGB);

        draw_quad(pass.output.name(), pass_output_size, pass.program.name());

        glDisable(GL_FRAMEBUFFER_SRGB);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        if( pass.sample_options.mipmap )
            glGenerateTextureMipmap(pass.output.texture().name());
    }

    // final output

    glBindTextureUnit(0, d->passes.back().output.texture().name());
    draw_quad(0, final_viewport_size, d->output_shader.name());

    // prepare for next frame

    for( auto& p : d->passes ) {
        if( p.feedback )
            std::swap(p.output, p.feedback);
    }
    ++d->frame_count;
}
