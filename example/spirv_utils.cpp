/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spirv_utils.h"

#include <glslang/Public/ResourceLimits.h>
#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <spirv_glsl.hpp>

#include <cstdio>

void initialize_glslang()
{
    glslang::InitializeProcess();
}

raslafx::spirv_shader compile_spirv(const char* vulkan_glsl, raslafx::ShaderStage glsl_stage)
{
    static const auto* defaultResources = GetDefaultResources();
    raslafx::spirv_shader spirv;

    const auto stage = (glsl_stage == raslafx::ShaderStage::Vertex) ? EShLangVertex : EShLangFragment;

    glslang::TShader shader(stage);
    shader.setStrings(&vulkan_glsl, 1);
    shader.setEnvInput(glslang::EShSourceGlsl, stage, glslang::EShClientVulkan, 100);
    shader.setEnvClient(glslang::EShClientVulkan, glslang::EShTargetVulkan_1_0);
    shader.setEnvTarget(glslang::EShTargetSpv, glslang::EShTargetSpv_1_0);
    if( !shader.parse(defaultResources, 100, false, EShMsgDefault) ) {
        std::fprintf(stderr, "shader compile error: %s\n", shader.getInfoLog());
        return {};
    }

    glslang::TProgram program;
    program.addShader(&shader);
    if( !program.link(EShMsgDefault) ) {
        std::fprintf(stderr, "shader link error: %s\n", program.getInfoLog());
        return {};
    }

    spv::SpvBuildLogger logger;
    glslang::SpvOptions spvOptions;
    glslang::GlslangToSpv(*program.getIntermediate(stage), spirv, &logger, &spvOptions);

    return spirv;
}

std::string transpile_spirv_to_opengl_glsl(const raslafx::spirv_shader& spirv)
{
    spirv_cross::CompilerGLSL glsl(spirv);

    spirv_cross::ShaderResources resources = glsl.get_shader_resources();

    if( !resources.uniform_buffers.empty() )
        glsl.set_decoration(resources.uniform_buffers[0].id, spv::DecorationBinding, StaticShaderUBOBinding);
    if( !resources.push_constant_buffers.empty() )
        glsl.set_decoration(resources.push_constant_buffers[0].id, spv::DecorationBinding, StaticShaderPushBinding);

    spirv_cross::CompilerGLSL::Options options;
    options.emit_push_constant_as_uniform_buffer = true;
    glsl.set_common_options(options);

    return glsl.compile();
}
