/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "png_reader.h"

#include <cstdio>
#include <cstring>
#include <png.h>

Image read_png_file(const char* path)
{
    Image result;

    png_image img = {};
    img.version = PNG_IMAGE_VERSION;

    if( png_image_begin_read_from_file(&img, path) == 0 ) {
        std::fprintf(stderr, "Error opening png file %s: %s", path, img.message);
        return {};
    }
    result.width = img.width;
    result.height = img.height;
    result.alpha = (img.format & PNG_FORMAT_FLAG_ALPHA) != 0;

    result.data.resize(img.width * img.height);

    img.format = PNG_FORMAT_RGBA;

    if( png_image_finish_read(&img, nullptr /*background*/, result.data.data(),
                              0 /*row_stride*/, nullptr /*colormap*/) == 0 ) {
        std::fprintf(stderr, "Error reading png file %s: %s", path, img.message);
    } else {
        result.ok = true;
    }

    if( img.opaque )
        png_image_free(&img);

    return result;
}
