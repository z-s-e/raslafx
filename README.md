# raslafx - RetroArch-slang preset and shader loading

A small C++17 library to load RA slang presets and shaders into an easy to use in-memory representation. The only dependency is SPIRV-Cross, which this library can optionally build together with itself (as a git submodule).

In contrast to more ambitious similar projects such as [librashader](https://github.com/SnowflakePowered/librashader) this does not come with runtimes that implement the fx pipeline rendering on top of any of the popular 3D APIs (GL/Vulkan/etc), although the bundled example app does implement a GL renderer for that as a reference. Beware that the example app is Linux only, and while the lib should in theory be platform agnostic, I've only tested under Linux.


## Why

I created this for myself, as a learning experience and to have a convenient way to try out RA shaders. But as I figure it might be useful to others it is published here. Bug reports are welcome, though I don't plan on adding more substantial features.


## Usage

Unless you need more control over the various steps of loading a preset, only the single function `raslafx::load_slang_preset` from the `<raslafx/raslafx.h>` header is needed. If the loading succeeded, the return value should be filled with a hopefully fairly self-explanatory pipeline description struct (assuming basic familiarity with RA slang presets and shaders).

One more note on the `raslafx::load_slang_preset` function; to minimize required dependencies I do not link against glslang directly, but make the user pass in a `compile_glsl_to_spirv` callback argument. The example code contains a wrapper function (`compile_spirv`) around glslang that can be used as reference.
