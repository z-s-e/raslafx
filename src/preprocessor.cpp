/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "raslafx/preprocessor.h"

#include "raslafx/basic_utils.h"

#include <algorithm>
#include <filesystem>

namespace raslafx {

constexpr size_t ShaderSizeLimit = 1024 * 1024;

struct preprocess_state {
    bool vertex_enabled = true;
    bool fragment_enabled = true;
};

static std::optional<Format> format_from_string(const std::string& s)
{
    using F = Format;
    if( s == "R8_UNORM" ) return F::R8_UNORM;
    if( s == "R8_UINT" ) return F::R8_UINT;
    if( s == "R8_SINT" ) return F::R8_SINT;
    if( s == "R8G8_UNORM" ) return F::R8G8_UNORM;
    if( s == "R8G8_UINT" ) return F::R8G8_UINT;
    if( s == "R8G8_SINT" ) return F::R8G8_SINT;
    if( s == "R8G8B8A8_UNORM" ) return F::R8G8B8A8_UNORM;
    if( s == "R8G8B8A8_UINT" ) return F::R8G8B8A8_UINT;
    if( s == "R8G8B8A8_SINT" ) return F::R8G8B8A8_SINT;
    if( s == "R8G8B8A8_SRGB" ) return F::R8G8B8A8_SRGB;
    if( s == "A2B10G10R10_UNORM_PACK32" ) return F::A2B10G10R10_UNORM_PACK32;
    if( s == "A2B10G10R10_UINT_PACK32" ) return F::A2B10G10R10_UINT_PACK32;
    if( s == "R16_UINT" ) return F::R16_UINT;
    if( s == "R16_SINT" ) return F::R16_SINT;
    if( s == "R16_SFLOAT" ) return F::R16_SFLOAT;
    if( s == "R16G16_UINT" ) return F::R16G16_UINT;
    if( s == "R16G16_SINT" ) return F::R16G16_SINT;
    if( s == "R16G16_SFLOAT" ) return F::R16G16_SFLOAT;
    if( s == "R16G16B16A16_UINT" ) return F::R16G16B16A16_UINT;
    if( s == "R16G16B16A16_SINT" ) return F::R16G16B16A16_SINT;
    if( s == "R16G16B16A16_SFLOAT" ) return F::R16G16B16A16_SFLOAT;
    if( s == "R32_UINT" ) return F::R32_UINT;
    if( s == "R32_SINT" ) return F::R32_SINT;
    if( s == "R32_SFLOAT" ) return F::R32_SFLOAT;
    if( s == "R32G32_UINT" ) return F::R32G32_UINT;
    if( s == "R32G32_SINT" ) return F::R32G32_SINT;
    if( s == "R32G32_SFLOAT" ) return F::R32G32_SFLOAT;
    if( s == "R32G32B32A32_UINT" ) return F::R32G32B32A32_UINT;
    if( s == "R32G32B32A32_SINT" ) return F::R32G32B32A32_SINT;
    if( s == "R32G32B32A32_SFLOAT" ) return F::R32G32B32A32_SFLOAT;
    return {};
}

static bool copy_to_destination(preprocessed_slang& result, bool& reset_copy_start, const preprocess_state& state,
                                const std::string& file, size_t copy_start, size_t count)
{
    if( state.fragment_enabled )
        result.shader_fragment.append(file.data() + copy_start, count);
    if( state.vertex_enabled )
        result.shader_vertex.append(file.data() + copy_start, count);
    if( result.shader_fragment.size() > ShaderSizeLimit || result.shader_vertex.size() > ShaderSizeLimit ) {
        log_error("shader size limit reached");
        return false;
    }
    reset_copy_start = true;
    return true;
}

static std::string read_string_after_space(const std::string& s, size_t& pos)
{
    while( s[pos] == ' ' )
        ++pos;
    const size_t start = pos;
    while( !end_of_line(s[pos]) )
        ++pos;
    return s.substr(start, pos - start);
}

static bool preprocess_recursive(preprocess_state& state, preprocessed_slang& result, const char* path, int recursion_limit = 32)
{
    if( recursion_limit == 0 ) {
        log_error("preprocess recursion limit reached");
        return false;
    }

    auto f = read_file(path);
    f.erase(std::remove(f.begin(), f.end(), '\r'), f.end());

    if( f.empty() ) {
        log_error("invalid file %s", path);
        return false;
    }

    size_t pos = 0;
    size_t copy_start = 0;
    bool reset_copy_start = false;

    constexpr std::string_view include_str("include ");
    constexpr std::string_view pragma_str("pragma ");
    constexpr std::string_view name_str("name ");
    constexpr std::string_view format_str("format ");
    constexpr std::string_view param_str("parameter ");
    constexpr std::string_view stage_str("stage ");
    constexpr std::string_view vertex_str("vertex");
    constexpr std::string_view fragment_str("fragment");

    while( true ) {
        const auto line_start = pos;
        reset_copy_start = false;

        if( f[pos] != '#' )
            goto move_to_next_line;
        ++pos;

        if( match_substring_at(f, pos, include_str) ) {
            pos += include_str.size();
            if( !copy_to_destination(result, reset_copy_start, state, f, copy_start, line_start - copy_start) )
                return false;

            size_t include_path_start = 0;
            for( int do_twice = 0; do_twice < 2; ++do_twice ) {
                include_path_start = pos;
                while( skip_to_quote(f[pos]) )
                    ++pos;
                if( f[pos] != '"' ) {
                    log_error("bad include line in %s, pos %lu", path, pos);
                    return false;
                }
                ++pos;
            }

            const auto parent_path = std::filesystem::path(path).parent_path();
            const auto include_path = parent_path / fixup_path(f.substr(include_path_start, pos - 1 - include_path_start));
            if( !preprocess_recursive(state, result, include_path.c_str(), recursion_limit - 1) )
                return false;
        } else if( match_substring_at(f, pos, pragma_str) ) {
            pos += pragma_str.size();

            if( match_substring_at(f, pos, param_str) ) {
                pos += param_str.size() - 1;
                if( !copy_to_destination(result, reset_copy_start, state, f, copy_start, line_start - copy_start) )
                    return false;

                char id_buf[64] = {};
                char desc[64] = {};
                float initial, minimum, maximum, step;
                int ret = std::sscanf(f.data() + pos, " %63s \"%63[^\"]\" %f %f %f %f",
                                      id_buf, desc, &initial, &minimum, &maximum, &step);

                if( ret != 5 && ret != 6 ) {
                    log_error("bad parameter pragma in %s, pos %lu", path, pos);
                    return false;
                }

                if( ret == 5 )
                    step = 0.1f * (maximum - minimum);

                std::string id(id_buf);
                if( auto it = result.parameters.find(id); it != result.parameters.end() ) {
                    if( it->second.desc != desc || it->second.initial != initial
                        || it->second.maximum != maximum || it->second.minimum != minimum || it->second.step != step) {
                        log_error("duplicated parameter mismatch in %s, pos %lu", path, pos);
                        return false;
                    }
                }

                result.parameters[id] = { {desc}, initial, minimum, maximum, step };
            } else if( match_substring_at(f, pos, stage_str) ) {
                pos += stage_str.size();
                if( !copy_to_destination(result, reset_copy_start, state, f, copy_start, line_start - copy_start) )
                    return false;

                bool ok = true;
                if( match_substring_at(f, pos, vertex_str) ) {
                    pos += vertex_str.size();
                    state.vertex_enabled = true;
                    state.fragment_enabled = false;
                } else if( match_substring_at(f, pos, fragment_str) ) {
                    pos += fragment_str.size();
                    state.vertex_enabled = false;
                    state.fragment_enabled = true;
                } else {
                    ok = false;
                }

                if( !ok || !end_of_line(f[pos]) ) {
                    log_error("bad stage pragma in %s, pos %lu", path, pos);
                    return false;
                }
            } else if( match_substring_at(f, pos, name_str) ) {
                pos += name_str.size();
                if( !copy_to_destination(result, reset_copy_start, state, f, copy_start, line_start - copy_start) )
                    return false;

                if( !result.name.empty() ) {
                    log_error("name redefinition in %s, pos %lu", path, pos);
                    return false;
                }

                result.name = read_string_after_space(f, pos);
            } else if( match_substring_at(f, pos, format_str) ) {
                pos += format_str.size();
                if( !copy_to_destination(result, reset_copy_start, state, f, copy_start, line_start - copy_start) )
                    return false;

                if( result.format ) {
                    log_error("format redefinition in %s, pos %lu", path, pos);
                    return false;
                }

                result.format = format_from_string(read_string_after_space(f, pos));

                if( !result.format ) {
                    log_error("bad format in %s, pos %lu", path, pos);
                    return false;
                }
            }
        }

move_to_next_line:
        while( !end_of_line(f[pos]) )
            ++pos;

        if( reset_copy_start )
            copy_start = pos;

        if( f[pos] == 0 )
            break;
        ++pos;
    }

    return copy_to_destination(result, reset_copy_start, state, f, copy_start, f.size() - copy_start);
}

std::optional<preprocessed_slang> preprocess(const char *path)
{
    preprocessed_slang result;
    preprocess_state state;
    if( preprocess_recursive(state, result, path) )
        return result;
    return {};
}

} // namespace
