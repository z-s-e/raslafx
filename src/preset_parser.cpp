/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "raslafx/preset_parser.h"

#include "raslafx/basic_utils.h"

#include <cctype>
#include <cstdlib>
#include <filesystem>

static constexpr size_t BufSize = 128;

namespace raslafx {

struct raw_root_preset {
    std::string path;
    std::unordered_map<std::string, std::string> entries;
};

static bool is_graph_noncomment_char(char c) { return std::isgraph(c) && c != '#'; }

static std::string preset_parse_raw_read_value(size_t& pos, const std::string& f)
{
    while( f[pos] == ' ' )
        ++pos;

    size_t value_start = pos;
    if( f[pos] == '"' ) {
        ++pos;
        value_start = pos;
        while( skip_to_quote(f[pos]) )
            ++pos;
    } else {
        while( is_graph_noncomment_char(f[pos]) )
            ++pos;
    }

    return f.substr(value_start, pos - value_start);
}

static bool preset_parse_read_bool(const std::string& s)
{
    return s == "true" || s == "1";
}

static std::optional<int> preset_parse_read_int(const std::string& s)
{
    char* str_end;
    int v = std::strtol(s.data(), &str_end, 10);
    return str_end == s.data() ? std::optional<int>() : std::optional<int>(v);
}

static std::optional<unsigned> preset_parse_read_uint(const std::string& s)
{
    char* str_end;
    unsigned v = std::strtoul(s.data(), &str_end, 10);
    return str_end == s.data() ? std::optional<unsigned>() : std::optional<unsigned>(v);
}

static float preset_parse_read_float(const std::string& s)
{
    return std::strtod(s.data(), nullptr);
}

static raw_root_preset preset_parse_raw(std::string path,
                                        bool parse_overridable_values_mode,
                                        std::unordered_map<std::string, parameter_info>* parameters,
                                        std::unordered_map<std::string, user_texture>* textures,
                                        int recursion_limit = 16)
{
    raw_root_preset result;
    result.path = path;

    if( recursion_limit == 0 ) {
        log_error("recursion limit reached at %s", path.data());
        return {};
    }

    const auto f = read_file(path.data());

    size_t pos = 0;

    while( true ) {
        const auto line_start = pos;
        std::string key;

        while( f[pos] == ' ' )
            ++pos;

        if( f[pos] == '#' ) {
            ++pos;

            constexpr std::string_view ref("reference ");

            if( pos == line_start + 1 && match_substring_at(f, pos, ref) ) {
                pos += ref.size();
                const auto ref_file = fixup_path(preset_parse_raw_read_value(pos, f));
                const auto ref_result = preset_parse_raw(std::filesystem::path(path).parent_path() / ref_file,
                                                         parse_overridable_values_mode,
                                                         parameters, textures, recursion_limit - 1);

                if( !parse_overridable_values_mode ) {
                    const bool ref_is_root = (ref_result.entries.find("shaders") != ref_result.entries.end());
                    if( !ref_is_root ) {
                        log_error("bad root reference in file %s", path.data());
                        return {};
                    }
                    return ref_result;
                }
            }

            goto move_to_next_line;
        }

        if( f[pos] == '\r' || end_of_line(f[pos]) )
            goto move_to_next_line;

        {
            const auto key_start = pos;
            while( is_graph_noncomment_char(f[pos]) )
                ++pos;

            key = f.substr(key_start, pos - key_start);
            if( key.empty() ) {
                log_error("empty key at position %lu, file %s", pos, path.data());
                goto move_to_next_line;
            }
        }

        while( f[pos] == ' ' )
            ++pos;

        if( f[pos] != '=' ) {
            log_error("missing = at position %lu, file %s", pos, path.data());
            goto move_to_next_line;
        }
        ++pos;

        if( parse_overridable_values_mode ) {
            if( auto it = parameters->find(key); it != parameters->end() )
                it->second.initial = preset_parse_read_float(preset_parse_raw_read_value(pos, f));
            else if( auto it = textures->find(key); it != textures->end() )
                it->second.path = std::filesystem::path(path).parent_path() / fixup_path(preset_parse_raw_read_value(pos, f));
        } else {
            result.entries[key] = preset_parse_raw_read_value(pos, f);
        }

move_to_next_line:
        while( !end_of_line(f[pos]) )
            ++pos;

        if( f[pos] == 0 )
            return result;
        ++pos;
    }
}

static texture_sample_options::WrapMode preset_parse_read_wrap_mode(const std::string& s)
{
    if( s == "clamp_to_edge" )
        return texture_sample_options::WrapMode::Edge;
    if( s == "repeat" )
        return texture_sample_options::WrapMode::Repeat;
    if( s == "mirrored_repeat" )
        return texture_sample_options::WrapMode::MirroredRepeat;
    return texture_sample_options::WrapMode::Border;
}

static render_pass_output_scaling::Mode preset_parse_read_scaling_mode(const std::string& s)
{
    if( s == "absolute" )
        return render_pass_output_scaling::Mode::Absolute;
    if( s == "viewport" )
        return render_pass_output_scaling::Mode::Viewport;
    return render_pass_output_scaling::Mode::Source;
}

static float preset_parse_read_scale_xy(const std::optional<std::string>& scale,
                                        render_pass_output_scaling::Mode mode,
                                        const char* fmt, unsigned i,
                                        const raw_root_preset& raw_root)
{
    char buf[BufSize];

    if( mode == render_pass_output_scaling::Mode::Absolute ) {
        if( scale ) {
            if( auto v = preset_parse_read_int(*scale); v )
                return *v;
        }

        snprintf(buf, sizeof(buf), fmt, i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
            if( auto v = preset_parse_read_int(it->second); v )
                return *v;
        }
    } else {
        if( scale )
            return preset_parse_read_float(*scale);

        snprintf(buf, sizeof(buf), fmt, i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
            return preset_parse_read_float(it->second);
    }

    return 1.0;
}

static void preset_parse_root_pass_scale(const raw_root_preset& raw_root, unsigned i, parsed_preset_render_pass& pass)
{
    char buf[BufSize];

    bool have_scale_type = false;
    std::string scale_type_x;
    std::string scale_type_y;

    snprintf(buf, sizeof(buf), "scale_type%u", i);
    if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
        have_scale_type = true;
        scale_type_x = it->second;
        scale_type_y = it->second;
    } else {
        snprintf(buf, sizeof(buf), "scale_type_x%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
            have_scale_type = true;
            scale_type_x = it->second;
        }

        snprintf(buf, sizeof(buf), "scale_type_y%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
            have_scale_type = true;
            scale_type_y = it->second;
        }
    }

    if( !have_scale_type )
        return;

    pass.x_scaling.mode = preset_parse_read_scaling_mode(scale_type_x);
    pass.y_scaling.mode = preset_parse_read_scaling_mode(scale_type_y);

    std::optional<std::string> scale;

    snprintf(buf, sizeof(buf), "scale%u", i);
    if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
        scale = it->second;

    pass.x_scaling.value = preset_parse_read_scale_xy(scale, pass.x_scaling.mode, "scale_x%u", i, raw_root);
    pass.y_scaling.value = preset_parse_read_scale_xy(scale, pass.y_scaling.mode, "scale_y%u", i, raw_root);
}

std::optional<parsed_root_preset> preset_parse_root(const char* path)
{
    parsed_root_preset result;

    const auto raw_root = preset_parse_raw(path, false, nullptr, nullptr);

    {
        auto it = raw_root.entries.find("shaders");
        if( it == raw_root.entries.end() ) {
            log_error("missing shaders entry in file %s", raw_root.path.data());
            return {};
        }
        auto v = preset_parse_read_uint(it->second);
        if( v && *v > 0 ) {
            result.passes.resize(*v);
        } else {
            log_error("bad shaders entry in file %s", raw_root.path.data());
            return {};
        }
    }

    //int feedback_pass = -1; -> apparently unused
    //if( auto it = raw_root.entries.find("feedback_pass"); it != raw_root.entries.end() ) {
    //    if( auto v = preset_parse_read_int(it->second); v )
    //        result.feedback_pass = *v;
    //}

    // read passes

    const auto raw_root_parent = std::filesystem::path(raw_root.path).parent_path();

    for( unsigned i = 0, N = result.passes.size(); i < N; ++i ) {
        auto& pass = result.passes[i];
        char buf[BufSize];

        snprintf(buf, sizeof(buf), "shader%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
            pass.path = raw_root_parent / fixup_path(it->second);
        } else {
            log_error("missing shader pass %s in file %s", buf, raw_root.path.data());
            return {};
        }

        snprintf(buf, sizeof(buf), "alias%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
            pass.alias = it->second;

        snprintf(buf, sizeof(buf), "frame_count_mod%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
            if( auto v = preset_parse_read_uint(it->second); v )
                pass.frame_count_mod = *v;
        }

        {
            // the sample options for the output of a pass are specified as the input option of the next pass
            // https://github.com/libretro/RetroArch/issues/14437
            auto& sample_options = (i == 0 ? result.input_sample_options : result.passes[i - 1].sample_options);

            snprintf(buf, sizeof(buf), "wrap_mode%u", i);
            if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
                sample_options.wrap = preset_parse_read_wrap_mode(it->second);

            snprintf(buf, sizeof(buf), "filter_linear%u", i);
            if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
                sample_options.linear_interpolation = preset_parse_read_bool(it->second);

            snprintf(buf, sizeof(buf), "mipmap_input%u", i);
            if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
                sample_options.mipmap = preset_parse_read_bool(it->second);
        }

        using Override = parsed_preset_render_pass::FormatOverride;

        snprintf(buf, sizeof(buf), "srgb_framebuffer%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() ) {
            if( preset_parse_read_bool(it->second) )
                pass.format_override = Override::SRGB;
        }

        snprintf(buf, sizeof(buf), "float_framebuffer%u", i);
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() && pass.format_override == Override::No ) {
            if( preset_parse_read_bool(it->second) )
                result.passes[i].format_override = Override::Float;
        }

        preset_parse_root_pass_scale(raw_root, i, pass);
    }

    // read textures

    if( auto it = raw_root.entries.find("textures"); it != raw_root.entries.end() ) {
        const auto& texture_list = it->second;
        unsigned next_start = 0;
        for( unsigned i = 0; i < texture_list.size(); ++i ) {
            if( texture_list[i] == ';' ) {
                if( i > next_start )
                    result.user_textures[texture_list.substr(next_start, i - next_start)] = {};
                next_start = i + 1;
            }
        }
        if( next_start < texture_list.size() )
            result.user_textures[texture_list.substr(next_start)] = {};
    }

    for( auto tex_it = result.user_textures.begin(); tex_it != result.user_textures.end(); ++tex_it ) {
        if( auto it = raw_root.entries.find(tex_it->first); it != raw_root.entries.end() )
            tex_it->second.path = raw_root_parent / fixup_path(it->second);

        char buf[BufSize];

        snprintf(buf, sizeof(buf), "%s_wrap_mode", tex_it->first.data());
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
            tex_it->second.sample_options.wrap = preset_parse_read_wrap_mode(it->second);

        snprintf(buf, sizeof(buf), "%s_linear", tex_it->first.data());
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
            tex_it->second.sample_options.linear_interpolation = preset_parse_read_bool(it->second);

        snprintf(buf, sizeof(buf), "%s_mipmap", tex_it->first.data());
        if( auto it = raw_root.entries.find(buf); it != raw_root.entries.end() )
            tex_it->second.sample_options.mipmap = preset_parse_read_bool(it->second);
    }

    result.path = raw_root.path;
    return result;
}

void preset_parse_overridable_values(const char *path,
                                     std::unordered_map<std::string, parameter_info>& parameters,
                                     std::unordered_map<std::string, user_texture>& textures)
{
    preset_parse_raw(path, true, &parameters, &textures);
}

} // namespace
