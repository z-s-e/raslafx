/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "raslafx/reflection.h"

#include "raslafx/basic_utils.h"

#include <spirv_cross.hpp>

#include <cstdlib>
#include <utility>

#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
#define ENABLE_EXCEPTION_HANDLING
#include <exception>
#endif

namespace raslafx {

static bool read_index_suffix(unsigned& result, const char* s)
{
    char* str_end;
    result = std::strtoul(s, &str_end, 10);
    if( str_end == s ) {
        log_error("shader has bad uniform index suffix");
        return false;
    }
    return true;
}

static bool reflect_ubo(uniform_buffer_info& result, unsigned pass, bool is_vertex,
                        const spirv_cross::Compiler& compiler, const spirv_cross::Resource& resource,
                        const shader_static_name_lookup_tables& lookup_tables)
{
    static constexpr std::string_view original_hist_size_prefix("OriginalHistorySize");
    static constexpr std::string_view pass_output_size_prefix("PassOutputSize");
    static constexpr std::string_view pass_feedback_size_prefix("PassFeedbackSize");

    if( is_vertex )
        result.stage_vertex = true;
    else
        result.stage_fragment = true;

    result.size = max(result.size, compiler.get_declared_struct_size(compiler.get_type(resource.base_type_id)));

    const auto ranges = compiler.get_active_buffer_ranges(resource.id);
    for( const auto& range : ranges ) {
        // we do less shader correctness checks here than RA does, and assume there are no mismatching uniforms,
        // so we can skip already seen members
        bool is_known_member = false;
        for( const auto& known_member : result.members ) {
            if( range.offset == unsigned(known_member.offset) ) {
                is_known_member = true;
                break;
            }
        }
        if( is_known_member )
            continue;

        const std::string& name = compiler.get_member_name(resource.base_type_id, range.index);
        uniform_data data;
        unsigned index_suffix;

        if( auto it = lookup_tables.uniforms.find(name); it != lookup_tables.uniforms.end() ) {
            data = it->second;
        } else if( name == "SourceSize" ) {
            const auto source = (pass == 0) ? shader_texture_source{shader_texture_source_original{0}}
                                            : shader_texture_source{shader_texture_source_pass_output{pass - 1, false}};
            data = uniform_data_texture_size{source};
        } else if( match_substring_at(name, 0, original_hist_size_prefix) ) {
            if( !read_index_suffix(index_suffix, name.data() + original_hist_size_prefix.size()) )
                return false;
            data = uniform_data_texture_size{shader_texture_source_original{index_suffix}};
        } else if( match_substring_at(name, 0, pass_output_size_prefix) ) {
            if( !read_index_suffix(index_suffix, name.data() + pass_output_size_prefix.size()) )
                return false;
            data = uniform_data_texture_size{shader_texture_source_pass_output{index_suffix, false}};
        } else if( match_substring_at(name, 0, pass_feedback_size_prefix) ) {
            if( !read_index_suffix(index_suffix, name.data() + pass_feedback_size_prefix.size()) )
                return false;
            data = uniform_data_texture_size{shader_texture_source_pass_output{index_suffix, true}};
        } else {
            log_error("shader has unknown uniform %s", name.data());
            return false;
        }

        // we also skip all the type and data overlap checks
        result.members.push_back({int(range.offset), data});
    }

    return true;
}

static bool reflect_texture(std::vector<shader_texture>& result, unsigned pass,
                            const spirv_cross::Compiler& compiler, const spirv_cross::Resource& image,
                            const shader_static_name_lookup_tables& lookup_tables)
{
    static constexpr std::string_view original_hist_prefix("OriginalHistory");
    static constexpr std::string_view pass_output_prefix("PassOutput");
    static constexpr std::string_view pass_feedback_prefix("PassFeedback");

    if( compiler.get_decoration(image.id, spv::DecorationDescriptorSet) != 0 ) {
        log_error("texture must use descriptor set 0");
        return false;
    }

    const auto& name = image.name;

    shader_texture_source source;
    unsigned index_suffix;

    if( auto it = lookup_tables.textures.find(name); it != lookup_tables.textures.end() ) {
        source = it->second;
    } else if( name == "Source" ) {
        source = (pass == 0) ? shader_texture_source{shader_texture_source_original{0}}
                             : shader_texture_source{shader_texture_source_pass_output{pass - 1, false}};
    } else if( match_substring_at(name, 0, original_hist_prefix) ) {
        if( !read_index_suffix(index_suffix, name.data() + original_hist_prefix.size()) )
            return false;
        source = shader_texture_source_original{index_suffix};
    } else if( match_substring_at(name, 0, pass_output_prefix) ) {
        if( !read_index_suffix(index_suffix, name.data() + pass_output_prefix.size()) )
            return false;
        if( index_suffix >= pass ) {
            log_error("shader references non-previous pass output");
            return false;
        }
        source = shader_texture_source_pass_output{index_suffix, false};
    } else if( match_substring_at(name, 0, pass_feedback_prefix) ) {
        if( !read_index_suffix(index_suffix, name.data() + pass_feedback_prefix.size()) )
            return false;
        source = shader_texture_source_pass_output{index_suffix, true};
    } else {
        log_error("shader has unknown texture %s", name.data());
        return false;
    }

    result.push_back({int(compiler.get_decoration(image.id, spv::DecorationBinding)), std::move(source)});
    return true;
}

static bool reflect_single_pass(render_pass_input& result, unsigned pass, const spirv_shader& shader, bool is_vertex,
                                const shader_static_name_lookup_tables& lookup_tables)
{
    const spirv_cross::Compiler compiler(shader);
    const auto res = compiler.get_shader_resources();

    if( !res.storage_buffers.empty() || !res.subpass_inputs.empty()
        || !res.storage_images.empty() || !res.atomic_counters.empty() ) {
        log_error("invalid shader resource type detected");
        return false;
    }

    if( res.uniform_buffers.size() > 1 || res.push_constant_buffers.size() > 1 ) {
        log_error("shader has too many uniform buffers");
        return false;
    }

    if( is_vertex ) {
        if( !res.sampled_images.empty() ) {
            log_error("textures not allowed in vertex shader");
            return false;
        }
        if( res.stage_inputs.size() != 2 ) {
            log_error("vertex shader input count mismatch");
            return false;
        }
        if( compiler.get_decoration(res.stage_inputs[0].id, spv::DecorationLocation) > 1
            || compiler.get_decoration(res.stage_inputs[1].id, spv::DecorationLocation) > 1 ) {
            log_error("vertex shader bad input locations");
            return false;
        }
    } else {
        if( res.stage_outputs.size() != 1 ) {
            log_error("multiple render targets not supported");
            return false;
        }
        if( compiler.get_decoration(res.stage_outputs[0].id, spv::DecorationLocation) != 0 ) {
            log_error("render target must use location = 0");
            return false;
        }
    }

    if( !res.uniform_buffers.empty() ) {
        const auto id = res.uniform_buffers[0].id;
        if( compiler.get_decoration(id, spv::DecorationDescriptorSet) != 0 ) {
            log_error("ubo must use descriptor set 0");
            return false;
        }

        auto binding = compiler.get_decoration(id, spv::DecorationBinding);
        if( result.ubo.binding == -1 )
            result.ubo.binding = binding;
        else if( result.ubo.binding != binding ) {
            log_error("ubo binding mismatch");
            return false;
        }

        if( !reflect_ubo(result.ubo, pass, is_vertex, compiler, res.uniform_buffers[0], lookup_tables) )
            return false;
    }

    if( !res.push_constant_buffers.empty() ) {
        if( !reflect_ubo(result.push_constant, pass, is_vertex, compiler, res.push_constant_buffers[0], lookup_tables) )
            return false;
    }

    for( const auto& image : res.sampled_images ) {
        if( !reflect_texture(result.textures, pass, compiler, image, lookup_tables) )
            return false;
    }

    return true;
}

shader_static_name_lookup_tables build_reflection_static_lookup_tables(const std::vector<pass_alias>& alias_table,
                                                                       const std::unordered_map<std::string, parameter_info>& parameters,
                                                                       const std::unordered_map<std::string, user_texture>& textures)
{
    shader_static_name_lookup_tables result;

    result.textures = { {"Original", shader_texture_source_original{0}} };
    result.uniforms = {
        {"MVP", uniform_data_mvp{}}, {"OutputSize", uniform_data_output_size{}},
        {"FinalViewportSize", uniform_data_final_viewport_size{}},
        {"FrameCount", uniform_data_frame_count{}}, {"FrameDirection", uniform_data_frame_direction{}},
        {"OriginalSize", uniform_data_texture_size{shader_texture_source_original{0}}}
    };

    for( const auto& alias_entry : alias_table ) {
        const auto alias_output = shader_texture_source_pass_output{alias_entry.index, false};
        const auto alias_feedback_output = shader_texture_source_pass_output{alias_entry.index, true};

        result.textures[alias_entry.alias] = alias_output;
        result.uniforms[alias_entry.alias + "Size"] = uniform_data_texture_size{alias_output};

        result.textures[alias_entry.alias + "Feedback"] = alias_feedback_output;
        result.uniforms[alias_entry.alias + "FeedbackSize"] = uniform_data_texture_size{alias_feedback_output};
    }

    for( auto it = textures.cbegin(), end = textures.cend(); it != end; ++it ) {
        const auto user = shader_texture_source_user{it->first};
        result.textures[it->first] = user;
        result.uniforms[it->first + "Size"] = uniform_data_texture_size{user};
    }

    for( auto it = parameters.cbegin(), end = parameters.cend(); it != end; ++it )
        result.uniforms[it->first] = uniform_data_parameter{it->first};

    return result;
}


std::optional<render_pass_input> reflect_render_pass(unsigned pass, const spirv_shader& vertex, const spirv_shader& fragment,
                                                     const shader_static_name_lookup_tables& lookup_tables)
{
    render_pass_input result;

#if defined(ENABLE_EXCEPTION_HANDLING)
    try {
#endif

        if( !reflect_single_pass(result, pass, vertex, true, lookup_tables) )
            return {};
        if( !reflect_single_pass(result, pass, fragment, false, lookup_tables) )
            return {};

#if defined(ENABLE_EXCEPTION_HANDLING)
    } catch(const std::exception& e) {
        log_error("spirv-cross exception: %s", e.what());
        return {};
    }
#endif

    return result;
}

} // namespace
