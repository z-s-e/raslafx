/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "raslafx/basic_utils.h"

#include <algorithm>
#include <cstdarg>
#include <cstdio>

namespace raslafx {

constexpr size_t FileSizeLimit = 4 * 1024 * 1024;

std::string read_file(const char *path)
{
    std::string result;

    auto f = std::fopen(path, "rb");
    if( f == nullptr ) {
        log_error("cannot open path %s for reading", path);
        return {};
    }

    std::fseek(f, 0L, SEEK_END);
    const long s = std::ftell(f);
    if( s < 0 ) {
        log_error("IO read error for path %s", path);
        std::fclose(f);
        return {};
    }
    if( size_t(s) > FileSizeLimit ) {
        log_error("file %s exeeds file size limit", path);
        std::fclose(f);
        return {};
    }

    result.resize(size_t(s));
    fseek(f, 0L, SEEK_SET);
    if( s > 0 && std::fread(result.data(), size_t(s), 1, f) != 1 ) {
        log_error("IO read error for path %s", path);
        std::fclose(f);
        return {};
    }

    std::fclose(f);
    return result;
}

std::string fixup_path(std::string p)
{
    std::replace(p.begin(), p.end(), '\\', '/');
    return p;
}

void log_error(const char* fmt, ...)
{
    std::fprintf(stderr, "raslafx error: ");
    va_list ap;
    va_start(ap, fmt);
    std::vfprintf(stderr, fmt, ap);
    va_end(ap);
    std::fprintf(stderr, "\n");
}

} // namespace
