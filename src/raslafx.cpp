/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "raslafx/raslafx.h"

#include "raslafx/basic_utils.h"
#include "raslafx/preprocessor.h"
#include "raslafx/preset_parser.h"
#include "raslafx/reflection.h"

#include <utility>

namespace raslafx {

struct feedback_and_history_requirements_visitor {
    pipeline_description& result;

    feedback_and_history_requirements_visitor(pipeline_description& r) : result(r)
    {
        for( const auto& pass : result.passes ) {
            for( const auto& u : pass.input.push_constant.members )
                std::visit(*this, u.data);
            for( const auto& u : pass.input.ubo.members )
                std::visit(*this, u.data);
            for( const auto& tex : pass.input.textures )
                std::visit(*this, tex.source);
        }
    }

    void operator()(const uniform_data_texture_size& u) { std::visit(*this, u.source); }

    void operator()(const shader_texture_source_original& tex_original)
    {
        result.history_required = max(result.history_required, tex_original.history);
    }

    void operator()(const shader_texture_source_pass_output& tex_pass)
    {
        if( tex_pass.feedback )
            result.passes[tex_pass.pass_index].feedback_required = true;
    }

    template<typename T> void operator()(const T&) {}
};

std::optional<pipeline_description> load_slang_preset(const char* path,
                                                      std::function<spirv_shader(const char *, ShaderStage)> compile_glsl_to_spirv)
{
    pipeline_description result;

    auto root_preset = preset_parse_root(path);
    if( !root_preset )
        return {};

    result.input_sample_options = root_preset->input_sample_options;
    result.user_textures = std::move(root_preset->user_textures);

    const auto pass_count = root_preset->passes.size();
    result.passes.resize(pass_count);

    std::vector<pass_alias> pass_alias_table;

    for( unsigned i = 0; i < pass_count; ++i ) {
        const auto& parsed_pass = root_preset->passes[i];
        auto& pass = result.passes[i];

        auto preprocessed = preprocess(parsed_pass.path.data());
        if( !preprocessed )
            return {};

        result.parameters.merge(std::move(preprocessed->parameters));

        pass.vertex = compile_glsl_to_spirv(preprocessed->shader_vertex.data(), ShaderStage::Vertex);
        if( pass.vertex.empty() ) {
            log_error("failed to compile vertex shader of %s to spirv", parsed_pass.path.data());
            return {};
        }
        pass.fragment = compile_glsl_to_spirv(preprocessed->shader_fragment.data(), ShaderStage::Fragment);
        if( pass.fragment.empty() ) {
            log_error("failed to compile fragment shader of %s to spirv", parsed_pass.path.data());
            return {};
        }

        if( parsed_pass.format_override == parsed_preset_render_pass::FormatOverride::SRGB )
            pass.format = Format::R8G8B8A8_SRGB;
        else if( parsed_pass.format_override == parsed_preset_render_pass::FormatOverride::Float )
            pass.format = Format::R16G16B16A16_SFLOAT;
        else if( preprocessed->format )
            pass.format = *preprocessed->format;

        pass.frame_count_mod = parsed_pass.frame_count_mod;
        pass.x_scaling = parsed_pass.x_scaling;
        pass.y_scaling = parsed_pass.y_scaling;
        pass.sample_options = parsed_pass.sample_options;

        if( !parsed_pass.alias.empty() )
            pass_alias_table.push_back({std::move(parsed_pass.alias), i});
        else if( !preprocessed->name.empty() )
            pass_alias_table.push_back({std::move(preprocessed->name), i});
    }

    preset_parse_overridable_values(path, result.parameters, result.user_textures);

    const auto lookup_tables = build_reflection_static_lookup_tables(pass_alias_table, result.parameters, result.user_textures);

    for( size_t i = 0; i < pass_count; ++i ) {
        auto& pass = result.passes[i];
        auto pass_input = reflect_render_pass(i, pass.vertex, pass.fragment, lookup_tables);
        if( !pass_input ) {
            log_error("failed to reflect shader %u", unsigned(i));
            return {};
        }
        pass.input = std::move(*pass_input);
    }

    feedback_and_history_requirements_visitor{result};

    return result;
}

} // namespace
