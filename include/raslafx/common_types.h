/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RASLAFX_COMMON_TYPES_H
#define RASLAFX_COMMON_TYPES_H

#include <stdint.h>
#include <string>
#include <variant>
#include <vector>

namespace raslafx {

enum class Format {
    /* 8-bit */
    R8_UNORM,
    R8_UINT,
    R8_SINT,
    R8G8_UNORM,
    R8G8_UINT,
    R8G8_SINT,
    R8G8B8A8_UNORM,
    R8G8B8A8_UINT,
    R8G8B8A8_SINT,
    R8G8B8A8_SRGB,

    /* 10-bit */
    A2B10G10R10_UNORM_PACK32,
    A2B10G10R10_UINT_PACK32,

    /* 16-bit */
    R16_UINT,
    R16_SINT,
    R16_SFLOAT,
    R16G16_UINT,
    R16G16_SINT,
    R16G16_SFLOAT,
    R16G16B16A16_UINT,
    R16G16B16A16_SINT,
    R16G16B16A16_SFLOAT,

    /* 32-bit */
    R32_UINT,
    R32_SINT,
    R32_SFLOAT,
    R32G32_UINT,
    R32G32_SINT,
    R32G32_SFLOAT,
    R32G32B32A32_UINT,
    R32G32B32A32_SINT,
    R32G32B32A32_SFLOAT,
};


struct parameter_info {
    std::string desc;
    float initial;
    float minimum;
    float maximum;
    float step;
};


struct texture_sample_options {
    enum class WrapMode { Border, Edge, Repeat, MirroredRepeat };

    WrapMode wrap = WrapMode::Border;
    bool linear_interpolation = false;
    bool mipmap = false;
};

struct user_texture {
    std::string path;
    texture_sample_options sample_options;
};

struct render_pass_output_scaling {
    enum class Mode { Default, Source, Absolute, Viewport };

    Mode mode = Mode::Default; // Default means Source for all passes except the last, which is Viewport
    float value = 1.0;
};


struct shader_texture_source_original {
    unsigned history = 0; // 0 is Original, 1 is OriginalHistory1 etc
};

struct shader_texture_source_pass_output {
    unsigned pass_index = 999; // silly default value to notice missing initialization
    bool feedback = false;
};

struct shader_texture_source_user {
    std::string name;
};

using shader_texture_source = std::variant<shader_texture_source_original, shader_texture_source_pass_output, shader_texture_source_user>;

struct shader_texture {
    int binding = -1;
    shader_texture_source source;
};


struct uniform_data_mvp {};
struct uniform_data_output_size {};
struct uniform_data_final_viewport_size {};
struct uniform_data_frame_count {};
struct uniform_data_frame_direction {};

struct uniform_data_parameter {
    std::string name;
};

struct uniform_data_texture_size {
    shader_texture_source source;
};

using uniform_data = std::variant<uniform_data_mvp, uniform_data_output_size, uniform_data_final_viewport_size,
                                  uniform_data_frame_count, uniform_data_frame_direction,
                                  uniform_data_parameter, uniform_data_texture_size>;

struct uniform_info {
    int offset = -1;
    uniform_data data;
};

struct uniform_buffer_info {
    int binding = -1;
    unsigned size = 0;
    bool stage_vertex = false;
    bool stage_fragment = false;
    std::vector<uniform_info> members;
};


struct render_pass_input {
    uniform_buffer_info ubo;
    uniform_buffer_info push_constant;
    std::vector<shader_texture> textures;
};


enum class ShaderStage { Vertex, Fragment };

using spirv_shader = std::vector<uint32_t>;


} // namespace

#endif // RASLAFX_COMMON_TYPES_H
