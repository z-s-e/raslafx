/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RASLAFX_WRAPPER_H
#define RASLAFX_WRAPPER_H

#include "common_types.h"

#include <functional>
#include <optional>
#include <unordered_map>

namespace raslafx {

struct render_pass_description {
    spirv_shader vertex;
    spirv_shader fragment;
    render_pass_input input;
    unsigned frame_count_mod = 0;
    Format format = Format::R8G8B8A8_UNORM;
    render_pass_output_scaling x_scaling;
    render_pass_output_scaling y_scaling;
    texture_sample_options sample_options;
    bool feedback_required = false;
};

struct pipeline_description {
    std::vector<render_pass_description> passes;
    std::unordered_map<std::string, parameter_info> parameters;
    std::unordered_map<std::string, user_texture> user_textures;
    texture_sample_options input_sample_options;
    unsigned history_required = 0;
};

std::optional<pipeline_description> load_slang_preset(const char* path,
                                                      std::function<spirv_shader(const char*, ShaderStage)> compile_glsl_to_spirv);

} // namespace

#endif // RASLAFX_WRAPPER_H
