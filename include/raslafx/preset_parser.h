/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RASLAFX_PRESET_PARSER_H
#define RASLAFX_PRESET_PARSER_H

#include "common_types.h"

#include <optional>
#include <unordered_map>

namespace raslafx {

struct parsed_preset_render_pass {
    enum class FormatOverride { No, SRGB, Float };

    std::string path;
    std::string alias;
    unsigned frame_count_mod = 0;
    texture_sample_options sample_options;
    FormatOverride format_override = FormatOverride::No;
    render_pass_output_scaling x_scaling;
    render_pass_output_scaling y_scaling;
};

struct parsed_root_preset {
    std::string path;
    std::vector<parsed_preset_render_pass> passes;
    std::unordered_map<std::string, user_texture> user_textures;
    texture_sample_options input_sample_options;
};

std::optional<parsed_root_preset> preset_parse_root(const char* path);
void preset_parse_overridable_values(const char* path,
                                     std::unordered_map<std::string, parameter_info>& parameters,
                                     std::unordered_map<std::string, user_texture>& textures);

} // namespace

#endif // RASLAFX_PRESET_PARSER_H
