/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RASLAFX_PREPROCESSOR_H
#define RASLAFX_PREPROCESSOR_H

#include "common_types.h"

#include <optional>
#include <unordered_map>

namespace raslafx {

struct preprocessed_slang {
    std::string name;
    std::optional<Format> format;
    std::unordered_map<std::string, parameter_info> parameters;
    std::string shader_vertex;
    std::string shader_fragment;
};

std::optional<preprocessed_slang> preprocess(const char* path);

} // namespace

#endif // RASLAFX_PREPROCESSOR_H

