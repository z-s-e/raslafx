/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RASLAFX_REFLECTION_H
#define RASLAFX_REFLECTION_H

#include "common_types.h"

#include <optional>
#include <unordered_map>

namespace raslafx {

struct pass_alias {
    std::string alias;
    unsigned index = 999;
};

struct shader_static_name_lookup_tables {
    std::unordered_map<std::string, shader_texture_source> textures;
    std::unordered_map<std::string, uniform_data> uniforms;
};

shader_static_name_lookup_tables build_reflection_static_lookup_tables(const std::vector<pass_alias>& alias_table,
                                                                       const std::unordered_map<std::string, parameter_info>& parameters,
                                                                       const std::unordered_map<std::string, user_texture>& textures);

std::optional<render_pass_input> reflect_render_pass(unsigned pass, const spirv_shader& vertex, const spirv_shader& fragment,
                                                     const shader_static_name_lookup_tables& lookup_tables);

} // namespace

#endif // RASLAFX_REFLECTION_H
