/* Copyright 2023 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RASLAFX_BASIC_UTILS_H
#define RASLAFX_BASIC_UTILS_H

#include <cstring>
#include <string>
#include <string_view>

namespace raslafx {

#ifdef __GNUC__
#define ATTR_VARARG_PRINTF( argnum ) __attribute__ (( format( __printf__, argnum, argnum + 1 )))
#else
#define ATTR_VARARG_PRINTF( argnum )
#endif

void log_error(const char* fmt, ...) ATTR_VARARG_PRINTF(1);

std::string read_file(const char* path);
std::string fixup_path(std::string p);

inline bool match_substring_at(const std::string& s, size_t pos, std::string_view sub)
{
    return (s.size() - pos < sub.size()) ? false : (std::memcmp(s.data() + pos, sub.data(), sub.size()) == 0);
}

inline bool end_of_line(char c) { return c == '\n' || c == 0; };
inline bool skip_to_quote(char c) { return c != '"' && c != '\n' && c != 0; };

inline unsigned max(unsigned a, unsigned b) { return a < b ? b : a; }

} // namespace

#endif // RASLAFX_BASIC_UTILS_H

